# MS DOS 8
## Using The App

using the app is simple just follow these steps

<!-- make a nnumbered list -->
1. download the app

    this is done  by running this command

<!-- code block -->
``` shell
git clone https://gitlab.com/360Gamer_J/ms-dos-8.git
```

2. open the app

    this is done by running this command

    ``` shell
    python3 startup.py
    ```

3. follow the instructions


> The Dealfult User Name is `admin` and the password is `admin`

